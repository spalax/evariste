.. _evariste:

Usage
=====

Here are the command line options for `evariste`.
Note that:

- other tools are installed together `evariste`: :ref:`evs`;
- you might be interested in the :ref:`logging plugins <plugin_logging>` to configure output.

.. argparse::
    :module: evariste.evs.compile.options
    :func: commandline_parser
    :prog: evariste
