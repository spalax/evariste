:mod:`evariste.plugins.vcs`
===========================

:class:`evariste.plugins.vcs`
-----------------------------

.. automodule:: evariste.plugins.vcs

:class:`evariste.plugins.none`
------------------------------

.. automodule:: evariste.plugins.vcs.none

:class:`evariste.plugins.git`
-----------------------------

.. automodule:: evariste.plugins.vcs.git
