:mod:`evariste.plugins.renderer`
================================

.. automodule:: evariste.plugins.renderer

This plugin does not define anything directly, but is interesting because of its submodules.

.. toctree::

   renderer/jinja2
   renderer/html
