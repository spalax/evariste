:mod:`evariste.plugins`
=======================

.. automodule:: evariste.plugins

The :class:`Plugin` class has a handful of subclasses.

.. toctree::

   plugins/action
   plugins/renderer
   plugins/vcs
