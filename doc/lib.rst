.. _lib:

Library documentation
=====================

.. currentmodule:: evariste

Modules, classes, functions and constants are documented here.

.. toctree::

   lib/builder
   lib/hooks
   lib/plugins
   lib/shared
   lib/tree
   lib/utils
