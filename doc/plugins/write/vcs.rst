.. _write_vcs:

Write VCS plugins
=================

.. currentmodule:: evariste.plugins.vcs

A VCS plugin is a subclass of :class:`VCS`, that must interpret its :func:`abstract methods <abc.abstractmethod>`.
