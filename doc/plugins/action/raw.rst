.. _plugin_action_raw:

``action.raw`` — Do not compile file: use file as-is
====================================================

The file is not compiled. This is a default plugin (enabled by default, and cannot be disabled). For any file, if no other :ref:`action plugin <plugin_action>` matches, then this one is used as last resort.
