Mandatory plugins
=================

The :data:`following plugins <evariste.plugins.MANDATORY_PLUGINS>` are mandatory: they are enabled by default, and cannot be disabled.
They are necessary for Évariste to run.

.. literalinclude:: /../evariste/plugins/__init__.py
   :start-at: MANDATORY_PLUGINS = {
   :end-at: }

End user do not interact directly with most of them. Otherwise, they are documented elsewhere in this documentation.
