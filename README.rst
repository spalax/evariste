Évariste 🍼 Recursively compile and publish a directory tree
============================================================

  On s'empressera de publier ses moindres observations pour peu qu'elles
  soient nouvelles et on ajoutera : « Je ne sais pas le reste. »

  -- Évariste Galois, 1831

Example
-------

I use this program to convert `this <https://framagit.org/lpaternault/cours-2-math>`__ (a repository of several hundreds of .tex files, with a few LibreOffice documents) to `that <https://lpaternault.frama.io/cours-2-math/>`__ (a web page representing the same directory structures, with the same files, along with their compiled (pdf) version, and some comments).

What's new?
-----------

See `changelog <https://framagit.org/spalax/evariste/blob/main/CHANGELOG.md>`_.

Download and install
--------------------

See https://evariste.readthedocs.io/en/latest/install/

Documentation
-------------

* The compiled documentation is available on `readthedocs <http://evariste.readthedocs.io>`_

* To compile it from source, download and run::

      cd doc && make html
